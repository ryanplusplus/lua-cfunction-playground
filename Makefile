LENV = /home/ryan/.lenv

LUA_LIB_DIR = $(LENV)/current/lib
LUA_INC_DIR = $(LENV)/current/include

CFLAGS  = -std=c99
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -pedantic
CFLAGS += -Werror
CFLAGS += -I$(LUA_INC_DIR)
CFLAGS += -L$(LUA_LIB_DIR)
CFLAGS += -llua

LIBS = -llua -lm -ldl

run: main.o
	@echo Running $<
	@./$<

clean:
	rm -rf *.o

main.o: main.c
	@echo Compiling $@
	@$(CC) $(CFLAGS) main.c -o main.o $(LIBS)
