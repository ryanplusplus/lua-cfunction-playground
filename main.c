#include <stdlib.h>
#include "lauxlib.h"
#include "lualib.h"

static int cfunction(lua_State *L) {
  int x = luaL_checkinteger(L, 1);
  int y = luaL_checkinteger(L, 2);
  lua_pushinteger(L, x + y);
  return 1;
}

int main(int argc, const char *argv[]) {
  (void)argc;
  (void)argv;

  lua_State *L = luaL_newstate();

  luaL_openlibs(L);

  luaL_loadfile(L, "main.lua");
  lua_call(L, 0, 1);

  lua_pushcfunction(L, cfunction);
  lua_call(L, 1, 0);

  lua_close(L);
}
